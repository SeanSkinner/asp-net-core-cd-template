# A simple template to enable continous deployment to Dockerhub (and beyond)

Copy the template .yml file provided in at the root of your repository.

1. Replace the name of the placeholder ({yourprojectfolderhere}) in the .yml file with the name of the folder your Dockerfile is contained in.
2. Add the relevant environmental variables to your CI/CD settings in GitLab.

## Environmental variables

- CI_REGISTRY: docker.io
- CI_REGISTRY_IMAGE: index.docker.io/yourdockerhubusernamehere/yourimagehere
- CI_REGISTRY_USER: yourdockerhubusernamehere
- CI_REGISTRY_PASSWORD: yourdockerhubpasswordhere
